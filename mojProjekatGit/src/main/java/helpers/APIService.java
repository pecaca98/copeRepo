package helpers;

import java.net.HttpURLConnection;
import java.net.URL;

public class APIService {

    public static HttpURLConnection sendGetRequest(String urlEndpoint) throws Exception {
        URL url = new URL(urlEndpoint);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        return httpURLConnection;
    }
}