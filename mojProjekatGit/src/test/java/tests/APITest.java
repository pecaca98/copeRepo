package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import java.net.HttpURLConnection;
import helpers.APIHelper;
import helpers.APIService;


public class APITest {

    private static final String API_ENDPOINT = "https://jsonplaceholder.typicode.com/posts/1";

    @Test
    public void testGetPost() throws Exception {
        // Pošaljite GET zahtev
        //ovaj komentar ulazi u master, SALE NE NESTO DODAO U PIPELINE
        HttpURLConnection connection = APIService.sendGetRequest(API_ENDPOINT);
        //Jesa je nesto menjao
        //Ana je nesto dodala pa je izmenila
        int responseCode = APIHelper.getResponseCode(connection);
        Assert.assertEquals(responseCode, 200, "Expected response dodala code to be 200");



        // Provera tela odgovora (sadržaja)
        String responseBody = APIHelper.getResponseBody(connection);
        Assert.assertTrue(responseBody.contains("userId"), "Expected response body to contain 'userId'");
        //prviComentar
        //komentar broj dva
        //Marko je dodao ovaj komentar broj 3
        //Cope je dodao ovaj komentar cetvrti 4
        //Moja grana dva dodaje komentar 5
        //Cope5 grana5 dodaje neki komentar pet 5
        //Cope6 dodaje komentar 6
        //COpe& dodaje komentar 7
        //Cope8 dodaje komentar 8 i nista vise
        //cope8 dodaje komentar zbog komita1
        //cope8 poslednji commit

    }
}

